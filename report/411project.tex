\documentclass[journal]{IEEEtran}
\let\beq\equation
\let\eeq\endequation
\let\beqa\eqnarray
\let\eeqa\endeqnarray

\usepackage{cite}
\usepackage[pdftex]{graphicx}
\graphicspath{{../pics/}}
\DeclareGraphicsExtensions{.eps,.pdf,.jpeg,.png}
\usepackage{amsmath}
\usepackage{array}
%\usepackage{auto-pst-pdf}
\usepackage{listings}
\lstset{breaklines=true, frame=single, basicstyle=\footnotesize}

\usepackage[caption=false,font=footnotesize]{subfig}
\hyphenation{op-tical net-works semi-conduc-tor}

\begin{document}
\title{ENEE411 Independent Circuit Design Project: \\ 
		Log Domain Filter in Subthreshold MOS}

\author{Nevo Magnezi\\
		University of Maryland, College Park\\
		E-mail: nmag@protonmail.com}

\markboth{Journal of ENEE411 Projects,~Vol.~10, No.~1, December-2017}
{Shell \MakeLowercase{\textit{et al.}}
	: Bare Demo of IEEEtran.cls for IEEE Journals}

\maketitle

\begin{abstract}
Log-Domain Filters are a class of current-mode circuits that have the benefit
large signal linearity and high signal to noise ratio. In this paper, I
	simulated Andreou 
	and Himmelbauer's low-pass log-domain filter. I constructed the circuit 
	using the 0.5 micron components from the class library, and ensured that
	all transistors are in
subthreshold operation. I altered the reference circuit by cascoding the
exponential building block used to transfer signal currents to 
logarithmically related voltages, to counteract the asymmetric presence of the
early effect that increases the gain for all frequencies. I verified that it is 
possible to tune the 3 decibel cut-off frequency via DC current sources and
implemented capacitors. 
\end{abstract}

\begin{IEEEkeywords}
Log-Domain Filters, Subthreshold, Large Signal, Current Mode
\end{IEEEkeywords}

\section{Introduction}

\IEEEPARstart{R}{esearchers} have studied log-domain filters since at
least 1979 \cite{adams1979filtering}, when Adams' proposed that any active
RC filter can turn into a log-domain filter by replacing resistors with
corresponding diodes (fig. \ref{adams}). An input current is logarithmicly turned into a
voltage via a forward biased diode, which is then buffered, filtered,
level-shifted and exponentiated into a current. Adams' proposed work, had major drawbacks,
however, including the
necessity of using operational amplifiers, which limit the range of the cut-off
frequency and contribute to additional phase shifting. Frey demonstrated a general design
strategy to implement a transfer function via the
log-domain approach, and realized the circuit using bipolar junction
transistors\cite{frey}. Himmelbauer and Andreou built on these and other
previous works to develop a log-domain filter for processing signal currents in
the nano-amp range in subthreshold MOS \cite{himmelbauer}. I simulated the
realization they provided for a first order low-pass filter using ngspice
revision 26, choosing to
replace the basic exponential building block (fig. \ref{exponential}) with a cascoded
version (fig. \ref{cascode}) in order to minimize dependence on the drain
voltage and impacts of the early effect. The simulated circuit is implemented
with 10 NMOS 20/3 and 2 PMOS 60/3 transistors, all in subthreshold operation. In
hindsight, implementing the capacitor as a MOSCAP may have also been
beneficial. Operating in subthreshold/weak inversion has the additional benefit of using a
fraction of the power than above threshold operation. Additionally, because the 
filtering occurs in the log-domain, low signal levels stay well above the noise
level and are less likely to be degraded, and large signal AC
signals can be easily processed \cite{himmelbauer}. 

\begin{figure}[!t]
\centering
\includegraphics[width=3in]{adams}
% where an .eps filename suffix will be assumed under latex, 
% and a .pdf suffix will be assumed for pdflatex; or what has been declared
% via \DeclareGraphicsExtensions.
	\caption{Adams' proposed basic log-domain filter \cite{adams1979filtering}}
\label{adams}
\end{figure}

%

\begin{figure}[!b]
\centering
	\subfloat[]{
		\includegraphics[width=1.5in]{exponential}
		\label{exponential}
		}
%	\hline
	\qquad
	\subfloat[]{
		\includegraphics[width=1.5in]{cascode}
		\label{cascode}
		}
		\caption{Basic (a) and cascoded (b) exponentiator/logger}
		\label{block}
\end{figure}


\section{Analysis}
In Himmelbauer's and Andreou's analysis, they assumed saturated, weak inversion
with infinite early voltage. Of course, early voltage is finite, and the
ultimate effect that was simulated is increasing gain at all frequencies. To
counteract this, I replaced the two basic exponentiating building blocks
(fig. \ref{block}), with cascoded versions. 
\subsection{Benefits of Cascoding}
The cascode transistors ensure that the source voltage of M3 is more dependent on the input current. Without the
cascodes, a increase in input current at M2 increases M2's gate and drain
voltages. M3 will experience the same increasing gate voltage. M3's drain voltage is both clamped to $V_{dd}$ and higher than that of the drain
of M2. Thus the current leaving M3's source will be distorted by the early
effect. In the case of the cascode, M3's drain voltage is no longer clamped at
a high voltage, and has freedom to change.  Similar analysis can be performed on the exponentiating side
of the circuit. 

\subsection{Function of Circuit}
Himmelbaur and Andreou determined that by setting the substrate voltage to the
lowest voltage (eliminating the body effect), a relationship between
the currents of the exponentiating/logging building block and the source
voltages can be found:
\begin{equation}
	I_B=I_A e^{\frac{V_A-V_B}{V_T}}
\end{equation}

where $A$ and $B$ correspond to the left and right sides of the building block,
respectively. 

With the addition of cascode transistors, this relationship is verified to be
more representational. By holding $V_A$ at ground, and $I_B$ at a selected bias
current $I_0$, we note that $I_0=I_A
e^{-\frac{V_B}{V_T}}$. Thus, $V_B = V_T log(\frac{I_A}{I_0})$. We are able to 
convert the log of a current into a voltage. Conversely, at the output of the
circuit, $V_B$ is grounded, and $I_A$ is set to $I_0$, so $I_B=I_0
e^{\frac{V_A}{V_T}}$. An exponent with the voltage as the power can be turned into a current. 
\section{Implementation}
To fully implement a log-domain filter, I simulated the circuit shown in fig.
\ref{schematic}. VA is a negative voltage to enable the current sources MB1 and
MB2 to be on. M1-M4 are the logging building block, converting the log of
the input current into a voltage at the source of M3. The higher the frequency
of the voltage at the source of M3, the more current is pulled through the
resistor. Thus, at higher frequencies, less current is mirrored across the
exponentiating block (M5-M8) on the right hand side. Thus the task of low-pass
filtering is performed, with the input and output being currents instead of
voltages, as in typical active RC filters. 
\begin{figure}[!t]

\centering
\includegraphics[width=3in]{schematic}
% where an .eps filename suffix will be assumed under latex, 
% and a .pdf suffix will be assumed for pdflatex; or what has been declared
% via \DeclareGraphicsExtensions.
	\caption{Schematic of simulated circuit (Iout points in wrong
	direction...oops)}
\label{schematic}
\end{figure}






\subsection{Determining Bias Current and Voltages}

To ensure the log relationship holds between current and voltage, the circuit
must have consistent logarithmic characteristics. Thus, the 
gate-to-source voltage should be below the threshold voltage, as the
logarithmic characteristics disappear at moderate inversion. I chose to
implement current sources as simple DC biased NMOS and PMOS transistors. By inspection

\begin{figure}[!h]
\centering
		\includegraphics[width=3in]{subthreshholdiv}
		\caption{$logID$ versus gate-source voltage for 20/3 NMOS, $V_{DS}=2.5$
		V}
		\label{subthreshold}
\end{figure}



of fig. \ref{subthreshold}, we can note that there is logarithmic IV
characteristics from $V_{GS}\approx0.4V$ to $V_{GS}\approx 1V$, or
correspondingly, a drain current range from 100pA to 10$\mu$A. I chose 10nA as
my bias current, corresponding to $V_{BIASN}=VA + 511.2\text{ mV}$. For the PMOS
current source, the bias current is still 10nA, corresponding to
$V_{BIASP}=V_{DD}- 733.6\text{ mV}$. For the NMOS current sources, the current
does have dependence on the drain voltage of transistors, which is variable.
However, the change in drain voltage is minimal, as the voltage varies as the log of the input
current). For the PMOS current source, the drain voltage varies very little as
well, as its change is comparable to the change in drain voltages of the NMOS
current sources. 

\subsection{Tuning}
As in \cite{himmelbauer}, 
\beq
f_{3dB}=\frac{I_0}{2 \pi C V_T}
\eeq
, where $I_0$ is the DC bias voltage, C is the value of the filter capacitor,
and $V_T$ is the thermal voltage. 

\section{Results}
\subsection{Operating Point}
With all DC current sources tuned to 10nA, and the DC input also being 10nA,
the simulation yielded a DC output current of 9.998 nA. The capacitor voltage
was found to be -43 $\mu$V, which is very close to the ideal value of zero.
Variations in these values is simply due to lack of accounting of significant
digits. For all intents and purposes, the DC output current is 10nA, and the DC
capacitor voltage is 0V. 

\subsection{AC Simulation with 3.3pF Capacitor}
In my simulation, a 3.3pF capacitor was initially used. This yields a
theoretical $f_{3dB}=37$kHz. The AC simulation results in figure \ref{mag_1}
shows unity gain until $f_{3dB}$, which occurs at $f_{3dB}=14.43$kHz, or about
half of the theoretical value. The error from the theoretical value is 61
percent, which may be accounted for due to lack of accounting for parameter
$\eta$, as well as the fact that the simulation temperature is 27 degrees C. Interestingly, the decibel slope does not look linear as it would look
with an active-RC. An approximation of the falling slope was done between 10MHz
and 100MHz and was found to be -43dB/dec. 
\begin{figure}[!h]
\centering
	\includegraphics[width=3in]{proj_magnitude}
	\caption{Magnitude of frequency response with C=3.3pF}
	\label{mag_1}
\end{figure}
In most of the passband, there is little to no phase shift, as can be seen in
figure \ref{phase_1}. Little phase shift is what is an ideal for a filter. Once
frequencies reach 1kHz and remain less than f-3dB, the phase will not change
any more than -50 degrees. If this filter was implemented as part of a system
that included negative feedback (like an op-amp or OTA), the change in phase
may contribute to instability for signals with frequencies above 1kHz. 
\begin{figure}[!h]
\centering
	\includegraphics[width=3in]{proj_phase}
	\caption{Phase of frequency response with C=3.3pF}
	\label{phase_1}
\end{figure}


\subsection{AC Simulation with 330fF Capacitor}
To see how the filter could be tuned, I divided the value of the capacitance by
10, C=330fF. The expectation is that the 3dB frequency would be multiplied by
10, for a value of 370 kHz. 

As can be seen in figure \ref{mag_2}, the 3-dB frequency increased to 115kHz,
or about 10 times the initial simulated value. The falling slope remains at
-43dB/dec. And once again, the phase response (figure \ref{phase_2}) remains
close to zero for most of the pass band (f $<$ 5kHz), however changes by as much
as -50 degrees as the frequency approaches the 3-dB frequency. 
\begin{figure}[!h]
	\centering
	\includegraphics[width=3in]{proj_magnitude}
	\caption{Magnitude of frequency response with C=330fF}
	\label{mag_2}
\end{figure}

\begin{figure}[!h]
	\centering
	\includegraphics[width=3in]{proj_phase}
	\caption{Phase of frequency response with C=330fF}
	\label{phase_2}
\end{figure}
\section{Conclusion}
The log-domain filter implemented in subthreshold MOSFET is a simple circuit to
construct. The benefits are the ability to use large signals as inputs, a high
signal-to-noise ratio, low power use due to subthreshold operation, and ease of
tunability (with even the option of electronic tuning). 
\appendix[SPICE netlist]

\lstinputlisting[firstline=1,firstnumber=1, xrightmargin=1pt]{../project_ideal.cir}
\bibliographystyle{IEEEtran}
\bibliography{411project}
\end{document}


